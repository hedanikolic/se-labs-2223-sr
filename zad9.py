import random

a = random.randint(0, 100)
b = -2
print(a)
print('Guess the number!')
while (b != a):
    b = int(input('Your guess: '))
    if b < a:
        print('Too low!')
    elif b > a:
        print('Too high!')
    else:
        print('You guessed correctly!')
        break
