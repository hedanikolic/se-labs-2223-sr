string = str(input('Enter a palindrome: '))
ans = string[::-1]

if ans == string:
    print('String is a palindrome.')
else:
    print('String is not a palindrome.')